// These string constants are used for styling console outputs

// srb - style red background, srt - style red text
export const srb =
    "font-weight: bold; background-color: #831010; color: white;";
export const srt = "font-weight: bold; color: #831010;";
// sgb - style green background, srt - style green text
export const sgb =
    "font-weight: bold; background-color: #438134; color: white;";
export const sgt = "font-weight: bold; color: #438134;";
// sbb - style black background
export const sbb = "font-weight: bold; background-color: black; color: white;";

// A utility function to make a random identifier of a certain character
// length.
export function utilityMakeId(length) {
    let result = "";
    const characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }
    return result;
}

export const jointInformation = Object.entries({
    q0: [
        'Kabine_zu_Radlauf2',
        0,
        [-Math.PI, Math.PI],
    ],
    q1: [
        'BaggerVerbindungArm2_zu_BaggerVerbindungArm',
        0,
        [-Math.PI / 4, Math.PI / 4],
    ],
    q2: [
        'ZylAusleger_zu_ZylAuslegerKolbenstange',
        -0.09868047363131909,
        [-0.2, 0.0],
    ],
    q3: [
        'ZylArm_zu_ZylArmKolbenstange',
        -0.04181359230784053,
        [-0.41, 0.0],
    ],
    q4: [
        'ZylLoeffel_zu_ZylLoeffelKolbenstange',
        -0.12725655458018606,
        [-0.32, 0.0],
    ],
    q5: [
        'ZylSchildKolbenstange_zu_ZylSchild',
        0.14,
        [0, 0.14],
    ],
});