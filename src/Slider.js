
import * as Paho from './vendor/mqttws31.js';
import { sgb, sgt, srb } from './utilities.js';

export default function JointSlider({jointName, jointRange, jointValue, setJointValue,
  mqttClient, mqttToken}) {

    function handleChangeJoint(value) {
        setJointValue(value);
        if (/^[0-9a-zA-Z]{10,20}$/.test(mqttToken.current)) {
            console.log(`%cTOKEN: ${mqttToken.current}`, sgb);
            const payload = JSON.stringify({
                command: 'setJointValue',
                jointName: jointName,
                jointValue: parseFloat(value)
            });
            console.log(`%cPAYLOAD: ${payload}`, sgt);
            const topic = `rwth/bcma/sessions/${ mqttToken.current }/command`;
            console.log(`%cTOPIC: ${topic}`, sgt);

            mqttClient.send(topic, payload);
            //console.log(`%cSENT: ${payload} to ${topic}`, sgb);
        } else {
            console.log(`%cINVALID TOKEN: ${mqttToken.current}`, srb);
        }
    }

    return (
        <div className='slider'>
            <label htmlFor="volume">{jointName} ({parseFloat(jointValue).toFixed(1)})</label>
            <input type="range" name={jointName} id={jointName} 
                onChange={(e) => handleChangeJoint(e.target.value)} 
                min={jointRange[0]} max={jointRange[1]} value={jointValue} 
                step={(jointRange[1] - jointRange[0]) / 100} />
        </div>
    );
}