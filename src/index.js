import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import * as Paho from './vendor/mqttws31.js';
import { utilityMakeId } from './utilities';

/*
 * Create MQTT Client Object
 * ----------------------------------------
 * The MQTT client is created using the Paho MQTT library.
 */

// Define and decode GET options passed to the single-page react application
const urlGetOptions = {
  local_broker: false,
};

// Parse GET options in a code block
{
  const urlSplit = window.location.href.split("?");
  if (urlSplit.length === 2) {
      let getParams = urlSplit[1];
      getParams = getParams.split("+");

      for (let param of getParams) {
          param = param.split("=");
          if (urlGetOptions[param[0]] !== undefined) {
              urlGetOptions[param[0]] =
                  (param.length === 1 && true) ||
                  (param.length === 2 && param[1]);
          }
      }
  }
}

console.dir(urlGetOptions);

const mqttConnectParams = [
  urlGetOptions.local_broker ? "localhost" : "broker.emqx.io",
  window.location.protocol === "https:" ? Number(8084) : Number(8083),
  urlGetOptions.local_broker ? "" : "/mqtt",
  urlGetOptions.local_broker ? 'bcmaclient' : `BCMA-MQTT-CLIENT-${utilityMakeId(6)}`,
];

const mqttClient = new Paho.MQTT.Client(...mqttConnectParams);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App mqttClient={mqttClient} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
