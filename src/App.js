import logo from "./logo.svg";
import "./App.css";
import JointSlider from "./Slider";
import { useState, useEffect, useRef } from "react";
import { sbb, jointInformation } from './utilities';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL
});

function App({ mqttClient }) {
    const [connected, setConnected] = useState(false);
    let token = useRef('undefined');

    const jointNames = jointInformation.map((item) => item[0]);
    const jointRanges = jointInformation.map((item) => item[1][2]);
    const [jointValues, setJointValues] = useState(
      jointInformation.map((item) => item[1][1]));

    function handleChangeToken(event) {
      token.current = event.target.value;
    }

    const brokerConnectOptions = {
        useSSL: window.location.protocol === "https:",
        onSuccess: onBrokerConnect,
    };

    function onBrokerConnect() {
        // Once a connection has been made, make a subscription and send a message.
        const payload = `%cConnected to broker.emqx.io`;
        console.log(payload, sbb);
        setConnected(true);
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log(`onConnectionLost: ${responseObject.errorMessage}`);
        }
        setConnected(false);

        console.log("Reconnecting...");
        mqttClient.connect(brokerConnectOptions);
    }

    mqttClient.onConnectionLost = onConnectionLost;

    useEffect(() => {
      //Runs only on the first render
      setTimeout(() => { mqttClient.connect(brokerConnectOptions) }, 1000);
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                <p>
                    <svg width='1em' height='1em' style={{display: 'inline', margin: '0 1em'}}>
                      <circle cx='0.5em' cy='0.5em' r='0.5em' 
                          fill={connected ? 'green' : 'red'} />
                    </svg>
                    {connected ? 'Connected' : 'Disconnected'}
                </p>
                <div className='App-sliders' style={{height: connected ? 'auto' : 0}}>
                  <p>Token from simulator:</p>
                  <input type="text" name="token" placeholder="token" onChange={handleChangeToken} />
                  <p>Joint positions:</p>
                  {
                    jointNames.map((name, index) => {
                      return (
                        <JointSlider key={name} jointName={name} 
                          mqttClient={mqttClient} mqttToken={token}
                          jointRange={jointRanges[index]} 
                          jointValue={jointValues[index]} 
                          setJointValue={(value) => setJointValues([...jointValues.slice(0, index), value, ...jointValues.slice(index + 1)])} />
                      )
                    })
                  }
                </div>
            </header>
        </div>
    );
}

export default App;
